package tests;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;

import junit.framework.TestCase;

import AST.PicoJavaParser;
import AST.PicoJavaScanner;
import beaver.Symbol;



public class MethodScannerTests extends TestCase {

	public void testMethodValidKeywords() {
		assertScannerOk("return");
	}

	public void testMethodValidSeparators() {
		assertScannerOk(",");
	}
	
	
	  // utilitity asserts to test scanner
	  
	  protected static void assertScannerOk(String s) {
	    try {
	      scan(s);
	    } catch (Throwable t) {
	      fail(t.getMessage());
	    }
	  }
	  
	  protected static void assertScannerError(String s) {
	    try {
	      scan(s);
	    } catch (Throwable t) {
	      return;
	    }
	    fail("Expected to find parse error in " + s);
	  }
	  
	  protected static void scan(String s) throws Throwable {
	    Reader reader = new StringReader(s);
	    PicoJavaScanner scanner = new PicoJavaScanner(new BufferedReader(reader));
	    Symbol symbol;
	    do {
	      symbol = scanner.nextToken();
	    } while (symbol.getId() != PicoJavaParser.Terminals.EOF);
	    reader.close();
	  }


}
