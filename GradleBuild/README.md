Gradle Build Description
========================

2018-06-15
Jesper &Ouml;qvist
Lund University, Sweden

This is a repackaging of [G&ouml;rel Hedins State Machine example][2] to use a
[Gradle][3] build instead of the Ant build of the original example project.

The advantages of using Gradle over Ant are:

* More flexibility in the builds by using [Groovy][5] code in the build tasks.
* Dependencies can be automatically downloaded from Maven repositories.
* Improved handling of file dependencies between build tasks.
* Build by convention: similar to Maven, Gradle assumes some sane default
  settings which minimizes the amount of configuration you need to do for each
  build.

In order to support building JastAdd projects with Gradle, a custom Gradle
plugin, [JastAddGradle][4], is used. The plugin is published to the JastAdd
Maven repository (`http://jastadd.org/mvn/`).


Directory Structure
-------------------

In order to make the state machine project a bit better structured the source
directories have been reorganized in the following manner:

- **src/main** the main Java sources (includes StateMachine compiler)
- **src/test** the Java test sources
- **src/gen** destination for generated Java code, removed by `gradle clean`
- **build** temporary directory used by Gradle for build artifacts, removed by
  `gradle clean`

Note that the exampleprogs directory moved into `src/main` and the tests
directory moved into `src/test`.

No additional Jar libraries are needed in the project from scratch since the
Gradle build fetches the required libraries dynamically. Thus, the `tools`
directory has been removed.

The Build Script
----------------

There are two new files used by the Gradle build:

- **build.gradle** the gradle build script
- **jastadd_modules** the JastAdd module definitions

The `jastadd_modules` file looks like this:

    module("statemachine") {
        java {
            basedir "."
            include "main/**/*.java"
            include "gen/**/*.java"
        }

        jastadd {
            include "spec/*.ast"
            include "spec/*.jadd"
            include "spec/*.jrag"
        }

        scanner {
            include "spec/*.flex"
        }

        parser {
            include "spec/*.parser"
        }
    }

This is a list of the files used by the parser, scanner and attribute code
generators. The JastAddGradle plugin uses these lists to run the various
code generators.

The `build.gradle` script is a little more complicated. It starts by listing
Gradle plugin dependencies:

    plugins {
      id 'java'
      id 'maven'
      id 'org.jastadd' version '1.13.0'
    }


The `java` and `maven` plugins are default Gradle plugins, and the
`org.jastadd` plugin is our custom JastAddGradle plugin.

The next part of the build script configures the JastAddGradle plugin by
telling it where to search for modules, which module to build, and some
other important build parameters:

    jastadd {
      configureModuleBuild()

      modules "jastadd_modules"
      module = "statemachine"

      astPackage = "AST"
      parser.name = "StateMachineParser"
    }


The JastAddGradle plugin adds default tasks to generate the scanner, parser,
and the abstract grammar.  The remaining parts of the build script are typical
for a Java Gradle build.


Testing
-------

You can run the tests using the following command:

    gradle test


The above runs the tests located in the `src/test/tests` directory.
The followig creates the Jar file `GradleBuild-all.jar`.

    gradle jarAll


Run the generated Jar file using this command:

    java -jar GradleBuild-all.jar exampleprogs/test.sm


[1]: http://link.springer.com/chapter/10.1007%2F978-3-642-18023-1_4
[2]: http://jastadd.org/web/examples.php?example=StateMachine
[3]: http://www.gradle.org/
[4]: https://bitbucket.org/joqvist/jastaddgradle
[5]: http://groovy.codehaus.org/
